package org.javaforever.infinity.complexverb;

import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Method;

public abstract class TwoDomainVerb implements Comparable<TwoDomainVerb>{
	protected Domain master;
	protected Domain slave;
	protected String label;
	protected String verbName;
	
	public abstract Method generateDaoImplMethod() throws Exception;
	public abstract String generateDaoImplMethodString() throws Exception;
	public abstract String generateDaoImplMethodStringWithSerial() throws Exception;
	public abstract Method generateDaoMethodDefinition() throws Exception;
	public abstract String generateDaoMethodDefinitionString() throws Exception;
	public abstract Method generateServiceMethodDefinition() throws Exception;
	public abstract String generateServiceMethodDefinitionString() throws Exception;
	public abstract Method generateServiceImplMethod() throws Exception;
	public abstract String generateServiceImplMethodString() throws Exception;
	public abstract String generateServiceImplMethodStringWithSerial() throws Exception;
	public abstract Method generateFacadeMethod() throws Exception;
	public abstract String generateFacadeMethodString() throws Exception;
	public abstract String generateFacadeMethodStringWithSerial() throws Exception;
	public Domain getMaster() {
		return master;
	}
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	
	@Override
	public int compareTo(TwoDomainVerb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
}
