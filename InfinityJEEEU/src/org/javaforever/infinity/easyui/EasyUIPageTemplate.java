package org.javaforever.infinity.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.DragonHideStatement;
import org.javaforever.infinity.domain.Dropdown;
import org.javaforever.infinity.domain.Facade;
import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.MenuItem;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.generator.JsonPagingGridJspTemplate;
import org.javaforever.infinity.generator.NamedS2SMJavascriptMethodGenerator;
import org.javaforever.infinity.include.JsonUserNav;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;
import org.javaforever.infinity.verb.Add;
import org.javaforever.infinity.verb.Delete;
import org.javaforever.infinity.verb.DeleteAll;
import org.javaforever.infinity.verb.FindById;
import org.javaforever.infinity.verb.FindByName;
import org.javaforever.infinity.verb.ListActive;
import org.javaforever.infinity.verb.ListAll;
import org.javaforever.infinity.verb.ListAllByPage;
import org.javaforever.infinity.verb.SearchByFieldsByPage;
import org.javaforever.infinity.verb.SearchByName;
import org.javaforever.infinity.verb.SoftDelete;
import org.javaforever.infinity.verb.SoftDeleteAll;
import org.javaforever.infinity.verb.Toggle;
import org.javaforever.infinity.verb.ToggleOne;
import org.javaforever.infinity.verb.Update;


public class EasyUIPageTemplate extends JsonPagingGridJspTemplate {
	protected Set<Facade> facades = new TreeSet<Facade>();
	protected JsonUserNav jsonUserNav;
	
	public JsonUserNav getJsonUserNav() {
		return jsonUserNav;
	}

	public void setJsonUserNav(JsonUserNav jsonUserNav) {
		this.jsonUserNav = jsonUserNav;
	}

	public Set<Facade> getFacades() {
		return facades;
	}

	public void setFacades(Set<Facade> facades) {
		this.facades = facades;
	}

	public EasyUIPageTemplate(){
		super();
	}
	public EasyUIPageTemplate(Domain domain) throws Exception{
		super();
		this.domain = domain;
		this.standardName = StringUtil.lowerFirst(domain.getPlural());
		this.verbs = new ArrayList<Verb>();
		this.verbs.add(new ListAll(domain));
		this.verbs.add(new ListActive(domain));
		this.verbs.add(new Delete(domain));
		this.verbs.add(new FindById(domain));
		this.verbs.add(new FindByName(domain));
		this.verbs.add(new SearchByName(domain));
		this.verbs.add(new SoftDelete(domain));
		this.verbs.add(new Update(domain));
		this.verbs.add(new Add(domain));
		this.verbs.add(new ListAllByPage(domain));
		for (Verb v : this.verbs){
			try {
				this.facades.add(new Facade(v, this.domain));
			} catch (ValidateException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Verb findVerb(String verbName){
		for (Verb v : this.verbs){
			if(v.getVerbName().equals(verbName))
				return v;
		}
		return null;
	}
	
	public String generateJspString(){
		return generateStatementList().getContent();
	}
	
	@Override
	public StatementList generateStatementList() {
		try {
			List<Writeable> sList =  new ArrayList<Writeable>();
			sList.add(new Statement(1000L,0,"<!DOCTYPE html>"));
			sList.add(new Statement(2000L,0,"<html>"));
			sList.add(new Statement(3000L,0,"<head>"));
			sList.add(new Statement(4000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />"));
			sList.add(new Statement(5000L,0,"<title>"+this.domain.getText()+"</title>"));
			sList.add(new Statement(6000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/default/easyui.css\">"));
			sList.add(new Statement(7000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/themes/icon.css\">"));
			sList.add(new Statement(8000L,0,"<link rel=\"stylesheet\" type=\"text/css\" href=\"../easyui/demo/demo.css\">"));
			sList.add(new Statement(9000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.min.js\"></script>"));
			sList.add(new Statement(10000L,0,"<script type=\"text/javascript\" src=\"../easyui/jquery.easyui.min.js\"></script>"));
			sList.add(new Statement(11000L,0,"</head>"));
			sList.add(new Statement(12000L,0,"<body class=\"easyui-layout\">"));
			sList.add(new Statement(13000L,0,"<div data-options=\"region:'north',border:false\" style=\"height:60px;background:#B3DFDA;padding:10px\"><h2>无垠式代码生成器生成结果</h2></div>"));
			sList.add(new Statement(14000L,0,"<div data-options=\"region:'west',split:true,title:'主菜单'\" style=\"width:156px;padding:0px;\">"));
			sList.add(new Statement(15000L,0,"<div class=\"easyui-accordion\" data-options=\"fit:true,border:false\">"));
			sList.add(new Statement(16000L,0,"<div title=\"域对象清单\" style=\"padding:0px\" data-options=\"selected:true\">"));
			sList.add(new Statement(17000L,0,"<div id=\"mmadmin\" data-options=\"inline:true\" style=\"width: 142px; height: 98%; overflow: hidden; left: 0px; top: 0px; outline: none; display: block;\" class=\"menu-top menu-inline menu easyui-fluid\" tabindex=\"0\"><div class=\"menu-line\" style=\"height: 122px;\"></div>"));
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">主页</div><div class=\"menu-icon icon-add\"></div></div>",!StringUtil.isBlank(this.allDomainList)&&this.allDomainList.size()>0&&!StringUtil.isBlank(this.allDomainList.get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.allDomainList.get(0).getLabel())));
			sList.add(new DragonHideStatement(17500L,0,"<div onclick=\"window.location='../pages/index.html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">Homepage</div><div class=\"menu-icon icon-add\"></div></div>",!(!StringUtil.isBlank(this.allDomainList)&&this.allDomainList.size()>0&&!StringUtil.isBlank(this.allDomainList.get(0).getLabel())&&!StringUtil.isEnglishAndDigitalAndEmpty(this.allDomainList.get(0).getLabel()))));
			long serial = 18000L;
			for (Domain d : this.allDomainList){
				sList.add(new Statement(serial,0,"<div onclick=\"window.location='../pages/"+d.getPlural().toLowerCase()+".html'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+d.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
				serial += 1000L;
			}
			for (MenuItem mi : this.menuItems){
				sList.add(new Statement(serial,0,"<div onclick=\"window.location='"+mi.getUrl()+"'\" class=\"menu-item\" style=\"height: 20px;\"><div class=\"menu-text\" style=\"height: 20px; line-height: 20px;\">"+mi.getText()+"</div><div class=\"menu-icon icon-add\"></div></div>"));
				serial += 1000L;
			}
			sList.add(new Statement(serial,0,"</div>"));
			sList.add(new Statement(serial+1000L,0,"</div>"));
			sList.add(new Statement(serial+2000L,0,"</div>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			sList.add(new Statement(serial+4000L,0,"<div data-options=\"region:'east',split:true,collapsed:true,title:'属性'\" style=\"width:250px;overflow: hidden\">"));
			sList.add(new Statement(serial+5000L,0,"</div>"));
			sList.add(new Statement(serial+6000L,0,"<div data-options=\"region:'south',border:false\" style=\"height:50px;background:#A9FACD;padding:10px;text-align: center\">火箭船软件工作室版权所有。作者电邮:jerry_shen_sjf@qq.com QQ群:277689737</div>"));
			sList.add(new Statement(serial+7000L,0,"<div data-options=\"region:'center',title:'"+this.domain.getText()+"清单'\">"));
			sList.add(new Statement(serial+8000L,0,"<div title=\"搜索面板\" class=\"easyui-panel\" style=\"width:1120px;height:200px\">"));
			sList.add(new Statement(serial+9000L,0,"<form id=\"ffsearch\" method=\"post\">"));
			sList.add(new Statement(serial+10000L,0,"<table cellpadding=\"5\">"));
			
			serial = serial + 11000L;
			List<Field> fields = new ArrayList<Field>();
			fields.addAll(this.domain.getFieldsWithoutId());
			Field f1,f2,f3;
			for (int i=0;i<fields.size();i=i+3){
				f1 = fields.get(i); 
				sList.add(new Statement(serial,0,"<tr>"));
				sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f1 instanceof Dropdown)));
				if (f1 instanceof Dropdown)
					sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f1).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f1).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../facade/listActive"+((Dropdown)f1).getTarget().getCapFirstPlural()+"Facade',loadFilter:function(data){return data.rows}\"/></td>",f1 instanceof Dropdown));
				if (i < fields.size()-1) {
					f2 = fields.get(i+1);
					sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f2 instanceof Dropdown)));					
					if (f2 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f2).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f2).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../facade/listActive"+((Dropdown)f2).getTarget().getCapFirstPlural()+"Facade',loadFilter:function(data){return data.rows}\"/></td>",f2 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+2000L,0,"<td>&nbsp;</td>"));
				}
				if (i < fields.size()-2) {
					f3 = fields.get(i+2);
					sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",!(f3 instanceof Dropdown)));					
					if (f3 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f3).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f3).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../facade/listActive"+((Dropdown)f3).getTarget().getCapFirstPlural()+"Facade',loadFilter:function(data){return data.rows}\"/></td>",f3 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+3000L,0,"<td>&nbsp;</td>"));
				}
				sList.add(new Statement(serial+4000L,0,"</tr>"));
				serial += 5000L;
			}
			sList.add(new Statement(serial,0,"<tr><td colspan=\"3\"><a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"search"+domain.getCapFirstPlural()+"ByFieldsByPage()\">搜索</a></td></tr>"));
			sList.add(new Statement(serial+1000L,0,"</table>"));
			sList.add(new Statement(serial+2000L,0,"</form>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			sList.add(new Statement(serial+4000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+"清单\" style=\"width:1120px;height:400px\" data-options=\"singleSelect:false,url:'../facade/search"+domain.getCapFirstPlural()+"ByFieldsByPageFacade',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
			sList.add(new Statement(serial+5000L,0,"<thead>"));
			sList.add(new Statement(serial+6000L,0,"<tr>"));
			sList.add(new Statement(serial+7000L,0,"<th data-options=\"field:'"+this.domain.getDomainId().getLowerFirstFieldName()+"',checkbox:true\">"+this.domain.getDomainId().getText()+"</th>"));
			serial = serial + 8000L;
			List<Field> fields2 = new ArrayList<Field>();
			fields2.addAll(this.domain.getFieldsWithoutId());
			for (Field f: fields2){
				sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>",!(f instanceof Dropdown)));
				if (f instanceof Dropdown)
					sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80,formatter:translate"+((Dropdown)f).getTarget().getCapFirstDomainName()+"\">"+f.getText()+"</th>",f instanceof Dropdown));
				serial+=1000L;
			}
			sList.add(new Statement(serial,0,"</tr>"));
			sList.add(new Statement(serial+1000L,0,"</thead>"));
			sList.add(new Statement(serial+2000L,0,"</table>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			sList.add(new Statement(serial+4000L,0,""));
			sList.add(new Statement(serial+5000L,0,"<div class=\"easyui-window\" title=\"新增"+this.domain.getText()+"\" id=\"wadd"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:550px;height:600px\">"));
			sList.add(new Statement(serial+6000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
			sList.add(new Statement(serial+7000L,0,"<form id=\"ff\" method=\"post\">"));
			sList.add(new Statement(serial+8000L,0,"<table cellpadding=\"5\">"));
			serial = serial + 9000L;
			List<Field> fields3 = new ArrayList<Field>();
			fields3.addAll(this.domain.getFieldsWithoutIdAndActive());
			for (Field f: fields3){
				sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",!(f instanceof Dropdown)));
				if (f instanceof Dropdown)
					sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../facade/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"Facade',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
				serial+=1000L;
			}
	        sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
			sList.add(new Statement(serial+1000L,0,"</table>"));
			sList.add(new Statement(serial+2000L,0,"</form>"));
			sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
			sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"add"+this.domain.getCapFirstDomainName()+"()\">新增</a>"));
			sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"clearForm()\">清除</a>"));
			sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wadd"+this.domain.getCapFirstDomainName()+"').window('close')\">取消</a>"));
			sList.add(new Statement(serial+7000L,0,"</div>"));
			sList.add(new Statement(serial+8000L,0,"</div>"));
			sList.add(new Statement(serial+9000L,0,""));
			sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"编辑"+this.domain.getText()+"\" id=\"wupdate"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:500px;height:500px\">"));
			sList.add(new Statement(serial+11000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
			sList.add(new Statement(serial+12000L,0,"<form id=\"ffedit\" method=\"post\">"));
			sList.add(new Statement(serial+13000L,0,"<input  type='hidden' name='"+this.domain.getDomainId().getLowerFirstFieldName()+"' id='"+this.domain.getDomainId().getLowerFirstFieldName()+"' value=''/>"));
			sList.add(new Statement(serial+14000L,0,"<table cellpadding=\"5\">"));
			serial = serial + 15000L;
			List<Field> fields4 = new ArrayList<Field>();
			fields4.addAll(this.domain.getFieldsWithoutIdAndActive());
			for (Field f: fields4){
				sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",!(f instanceof Dropdown)));
				if (f instanceof Dropdown)
					sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../facade/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"Facade',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
				serial+=1000L;
			}
			sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
			sList.add(new Statement(serial+1000L,0,"</table>"));
			sList.add(new Statement(serial+2000L,0,"</form>"));
			sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
			sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"update"+this.domain.getCapFirstDomainName()+"()\">编辑</a>"));
			sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#ffedit').form('clear');\">清除</a>"));
			sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wupdate"+this.domain.getCapFirstDomainName()+"').window('close')\">取消</a>"));
			sList.add(new Statement(serial+7000L,0,"</div>"));
			sList.add(new Statement(serial+8000L,0,"</div>"));		
			sList.add(new Statement(serial+9000L,0,"</body>"));
			sList.add(new Statement(serial+9000L,0,"<script type=\"text/javascript\">"));
			sList.add(new Statement(serial+10000L,0,"var params = {};"));			
			sList.add(new Statement(serial+11000L,0,"var pagesize = 10;"));
			sList.add(new Statement(serial+12000L,0,"var pagenum = 1;"));
			sList.add(new Statement(serial+13000L,0,"var toolbar = ["));
			StatementList  slAdd =((EasyUIPositions)new Add(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slAdd.setSerial(serial+13100L);
			sList.add(slAdd);
			sList.add(new Statement(serial+13150L,0,","));
			StatementList  slUpdate =((EasyUIPositions)new Update(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slUpdate.setSerial(serial+13200L);
			sList.add(slUpdate);
			sList.add(new Statement(serial+13250L,0,","));
			StatementList  slSoftDelete =((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slSoftDelete.setSerial(serial+13300L);
			sList.add(slSoftDelete);
			sList.add(new Statement(serial+13350L,0,","));
			StatementList  slDelete =((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slDelete.setSerial(serial+13400L);
			sList.add(slDelete);
			sList.add(new Statement(serial+13450L,0,","));
			StatementList  slToggle =((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slToggle.setSerial(serial+13500L);
			sList.add(slToggle);
			sList.add(new Statement(serial+13550L,0,","));
			StatementList  slToggleOne =((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slToggleOne.setSerial(serial+13600L);
			sList.add(slToggleOne);
			sList.add(new Statement(serial+13650L,0,",'-',"));
			StatementList  slSoftDeleteAll =((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slSoftDeleteAll.setSerial(serial+13700L);
			sList.add(slSoftDeleteAll);
			sList.add(new Statement(serial+13750L,0,","));
			StatementList  slDeleteAll =((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSButtonBlock().getMethodStatementList();
			slDeleteAll.setSerial(serial+13800L);
			sList.add(slDeleteAll);			
			sList.add(new Statement(serial+14000L,0,"];"));
			sList.add(new Statement(serial+15000L,0,"$(document).ready(function(){"));
			sList.add(new Statement(serial+16000L,0,"$(\"#dg\").datagrid(\"load\");"));
			sList.add(new Statement(serial+17000L,0,"});"));

			sList.add(new Statement(serial+18000L,0,"function clearForm(){"));
			sList.add(new Statement(serial+19000L,0,"$('#ff').form('clear');"));
			sList.add(new Statement(serial+20000L,0,"}"));

			sList.add(((EasyUIPositions)new Add(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+21000L,0));
			sList.add(((EasyUIPositions)new Update(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+22000L,0));
			sList.add(((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+23000L,0));
			sList.add(((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+24000L,0));
			sList.add(((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+25000L,0));
			sList.add(((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+26000L,0));
			sList.add(((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+27000L,0));
			sList.add(((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+28000L,0));
			sList.add(((EasyUIPositions)new SearchByFieldsByPage(this.domain)).generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(serial+29000L,0));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanMethod().generateMethodStatementList(serial+30000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+31000L));
			
			serial = serial+32000L;
			List<Field> fields5 = new ArrayList<Field>();
			fields5.addAll(this.domain.getFieldsWithoutIdAndActive());
			for (Field f: fields4){
				if (f instanceof Dropdown){
					sList.add(((Dropdown)f).generateTranslateMethod().generateMethodStatementList(serial));
					serial+=1000L;
				}
			}
			
			sList.add(new Statement(serial,0,"</script>"));
			sList.add(new Statement(serial+1000L,0,"</html>"));
			StatementList mylist = WriteableUtil.merge(sList);
			return mylist;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
