package org.javaforever.infinity.verb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.JavascriptBlock;
import org.javaforever.infinity.domain.JavascriptMethod;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.domain.Signature;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.easyui.EasyUIPositions;
import org.javaforever.infinity.generator.MybatisSqlReflector;
import org.javaforever.infinity.generator.NamedS2SMStatementGenerator;
import org.javaforever.infinity.generator.NamedS2SMStatementListGenerator;
import org.javaforever.infinity.generator.NamedStatementGenerator;
import org.javaforever.infinity.generator.NamedStatementListGenerator;
import org.javaforever.infinity.utils.InterVarUtil;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;

public class Toggle extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("toggle"+StringUtil.capFirst(this.domain.getStandardName()));	
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport("java.sql.PreparedStatement");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2,this.domain.getDomainId().getFieldName(),this.domain.getDomainId().getRawType()));
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,2,"String query = \""+MybatisSqlReflector.generateToggleSqlForJDBC(domain)+"\";"));
		list.add(new Statement(200L,2,"PreparedStatement ps = connection.prepareStatement(query);"));
		list.add(new Statement(300L,2,"ps.setLong(1,"+this.domain.getDomainId().getLowerFirstFieldName()+");"));
		list.add(new Statement(400L,2,"int result = ps.executeUpdate();"));
		list.add(new Statement(500L,2,"if (result > 0) {"));
		list.add(new Statement(600L,3,"return true;"));
		list.add(new Statement(700L,2,"}"));
		list.add(new Statement(800L,2,"return false;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("toggle"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2,this.domain.getDomainId().getFieldName(),this.domain.getDomainId().getClassType()));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception{
		return generateDaoImplMethod().generateMethodString();
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception{
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception{
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("toggle"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,this.domain.getDomainId().getFieldName(),this.domain.getDomainId().getClassType()));
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception{
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() throws Exception{
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception{
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("toggle"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".database.DBConf");
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".daoimpl."+this.domain.getStandardName()+"DaoImpl");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getDomainId().getFieldName(),this.domain.getDomainId().getRawType()));
		//Service method
		Method daomethod = this.generateDaoMethodDefinition();
				
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementListGenerator.generateServiceImplReturnBoolean(1000L, 2, InterVarUtil.DB.connection, InterVarUtil.DB.dbconf, InterVarUtil.DB.dao, daomethod));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception{
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception{
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	public Toggle(){
		super();
		this.setLabel("切换");
	}
	
	public Toggle(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("Toggle"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("切换");
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception{
		return null;
	}

	@Override
	public Method generateFacadeMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("processRequest");
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		List<String> list = new ArrayList<String>();
		list.add("ServletException");
		list.add("IOException");
		method.setIsprotected(true);
		method.setOtherExceptions(list);
		method.addSignature(new Signature(1,"request",new Type("HttpServletRequest","javax.servlet.http")));
		method.addSignature(new Signature(2,"response",new Type("HttpServletResponse","javax.servlet.http")));
		method.addAdditionalImport("java.io.IOException");
		method.addAdditionalImport("java.io.PrintWriter");
		method.addAdditionalImport("javax.servlet.ServletException");
		method.addAdditionalImport("javax.servlet.http.HttpServlet");
		method.addAdditionalImport("javax.servlet.http.HttpServletRequest");
		method.addAdditionalImport("javax.servlet.http.HttpServletResponse");
		method.addAdditionalImport("java.util.Map");
		method.addAdditionalImport("java.util.TreeMap");
		method.addAdditionalImport("net.sf.json.JSONObject");
		//method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addAdditionalImport(this.domain.getPackageToken()+".serviceimpl."+this.domain.getStandardName()+"ServiceImpl");
		
		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		wlist.add(NamedStatementGenerator.getFacadeSetContentType(1000L, 2, InterVarUtil.Servlet.response, InterVarUtil.SimpleJEE.UTF8.getVarName()));
		wlist.add(NamedStatementGenerator.getControllerPrintWriterOut(2000L, 2, InterVarUtil.Servlet.response, InterVarUtil.Servlet.out));
		wlist.add(NamedStatementGenerator.getJsonResultMap(3000L, 2, resultMap));
		wlist.add(NamedStatementGenerator.getTryHead(4000L, 2));
		//wlist.add(NamedStatementGenerator.getPrepareDomainVarInit(5000L, 2, this.domain));
		wlist.add(NamedStatementGenerator.getSetDomainIdFromRequest(6000L, 3, this.domain, InterVarUtil.Servlet.request));
		wlist.add(NamedStatementGenerator.getPrepareService(7000L,3, service));
		wlist.add(NamedStatementGenerator.getCallServiceMethodAssignSuccess(8000L, 3, service, generateServiceMethodDefinition()));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAssignAndNull(9000L, 3, resultMap));
		wlist.add(NamedStatementGenerator.getEncodeMapToJsonResultMap(10000L, 3, resultMap,InterVarUtil.Servlet.out));
		wlist.add(NamedStatementListGenerator.generateCatchExceptionPrintStackPrintJsonMapFinallyCloseOutFooter(11000L, 2, InterVarUtil.Servlet.response,resultMap, InterVarUtil.Servlet.out));
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() throws Exception{
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception{
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("toggle"+domain.getCapFirstDomainName());
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "{"));
		sl.add(new Statement(2000,1, "text:'切换',"));
		sl.add(new Statement(3000,1, "iconCls:'icon-cut',"));
		sl.add(new Statement(4000,1, "handler:function(){ "));
		sl.add(new Statement(5000,2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
		sl.add(new Statement(6000,2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
		sl.add(new Statement(7000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(8000,3, "return;"));
		sl.add(new Statement(9000,2, "}"));
		sl.add(new Statement(10000,2, "if (rows.length > 1) {"));
		sl.add(new Statement(11000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(12000,3, "return;"));
		sl.add(new Statement(13000,2, "}"));
		sl.add(new Statement(14000,2, "var "+domain.getDomainId().getLowerFirstFieldName()+" = rows[0][\""+domain.getDomainId().getLowerFirstFieldName()+"\"];"));
		sl.add(new Statement(15000,2, "toggle"+this.domain.getCapFirstDomainName()+"("+domain.getDomainId().getLowerFirstFieldName()+");"));
		sl.add(new Statement(16000,1, "}"));
		sl.add(new Statement(17000,0, "}"));
		block.setMethodStatementList(sl);
		return block;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		Domain domain = this.domain;
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("toggle"+domain.getCapFirstDomainName());
		Signature s1 = new Signature();
		s1.setName(domain.getDomainId().getLowerFirstFieldName());
		s1.setPosition(1);
		s1.setType(new Type("var"));
		method.addSignature(s1);
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "$.ajax({"));
		sl.add(new Statement(2000,2, "type: \"post\","));
		sl.add(new Statement(3000,2, "url: \"../facade/toggle"+domain.getCapFirstDomainName()+"Facade\","));
		
		sl.add(new Statement(4000,2, "data: {"));
		sl.add(new Statement(5000,3, "\""+domain.getDomainId().getLowerFirstFieldName()+"\":"+domain.getDomainId().getLowerFirstFieldName()+""));
		sl.add(new Statement(6000,2, "},"));
		sl.add(new Statement(7000,2, "dataType: 'json',"));
		sl.add(new Statement(8000,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(9000,3, "$(\"#dg\").datagrid(\"load\");"));
		sl.add(new Statement(10000,2, "},"));
		sl.add(new Statement(11000,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(12000,2, "},"));
		sl.add(new Statement(13000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(14000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(15000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(16000,2, "}"));
		sl.add(new Statement(17000,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;		
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
	}

}
