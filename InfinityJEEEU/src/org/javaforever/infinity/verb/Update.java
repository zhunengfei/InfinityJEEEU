package org.javaforever.infinity.verb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Dropdown;
import org.javaforever.infinity.domain.Field;
import org.javaforever.infinity.domain.JavascriptBlock;
import org.javaforever.infinity.domain.JavascriptMethod;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.domain.Signature;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.easyui.EasyUIPositions;
import org.javaforever.infinity.generator.NamedStatementGenerator;
import org.javaforever.infinity.generator.NamedStatementListGenerator;
import org.javaforever.infinity.utils.InterVarUtil;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;

public class Update extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("java.util.ArrayList");
		method.addAdditionalImport("java.sql.PreparedStatement");
		method.addAdditionalImport("java.sql.ResultSet");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementGenerator.getUpdateSqlStatement(1000L,2, this.domain,InterVarUtil.DB.query));
		list.add(NamedStatementGenerator.getPrepareStatement(2000L,2, InterVarUtil.DB.ps, InterVarUtil.DB.query, InterVarUtil.DB.connection));
		list.add(NamedStatementListGenerator.generatePsSetDomainFields(3000L, 2,this.domain, InterVarUtil.DB.ps));
		list.add(NamedStatementGenerator.getPrepareStatementExcuteUpdate(4000L,2, InterVarUtil.DB.result, InterVarUtil.DB.ps));
		list.add(NamedStatementListGenerator.generateResultReturnSuccess(5000L,2, InterVarUtil.DB.result));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public Method generateDaoMethodDefinition() {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() {
		return generateDaoImplMethod().generateMethodString();
	}

	@Override
	public String generateDaoMethodDefinitionString() {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() {
		return generateDaoImplMethod().generateMethodContentStringWithSerial();
	}

	@Override
	public Method generateServiceMethodDefinition() {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() {
		Method method = new Method();
		method.setStandardName("processRequest");
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		List<String> list = new ArrayList<String>();
		list.add("ServletException");
		list.add("IOException");
		method.setIsprotected(true);
		method.setOtherExceptions(list);
		method.addSignature(new Signature(1,"request",new Type("HttpServletRequest","javax.servlet.http")));
		method.addSignature(new Signature(2,"response",new Type("HttpServletResponse","javax.servlet.http")));
		method.addAdditionalImport("java.io.IOException");
		method.addAdditionalImport("java.io.PrintWriter");
		method.addAdditionalImport("javax.servlet.ServletException");
		method.addAdditionalImport("javax.servlet.http.HttpServlet");
		method.addAdditionalImport("javax.servlet.http.HttpServletRequest");
		method.addAdditionalImport("javax.servlet.http.HttpServletResponse");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addAdditionalImport(this.domain.getPackageToken()+".serviceimpl."+this.domain.getStandardName()+"ServiceImpl");
		
		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		wlist.add(NamedStatementGenerator.getControllerSetContentType(1000L, 2, InterVarUtil.Servlet.response, InterVarUtil.SimpleJEE.UTF8.getVarName()));
		wlist.add(NamedStatementGenerator.getControllerPrintWriterOut(2000L, 2, InterVarUtil.Servlet.response, InterVarUtil.Servlet.out));
		wlist.add(NamedStatementGenerator.getTryHead(3000L, 2));
		wlist.add(NamedStatementGenerator.getPrepareDomainVarInit(4000L, 3, this.domain));
		wlist.add(NamedStatementListGenerator.generateSetDomainDataFromRequest(5000L, 3, this.domain, InterVarUtil.Servlet.request));
		wlist.add(NamedStatementGenerator.getPrepareService(7000L,3, service));
		wlist.add(NamedStatementGenerator.getCallServiceMethod(8000L, 3, service, generateServiceMethodDefinition()));
		wlist.add(NamedStatementGenerator.getResponseRedirectUrl(9000L, 3, InterVarUtil.Servlet.response, "../controller/listAll"+this.domain.getPlural()+"Controller"));
		wlist.add(NamedStatementListGenerator.generateCatchExceptionPrintStackRedirectUrlFinallyCloseOutFooter(10000L, 2, InterVarUtil.Servlet.response,"../controller/listAll"+this.domain.getPlural()+"Controller", InterVarUtil.Servlet.out));
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}
	

	@Override
	public String generateControllerMethodString() {
		return generateControllerMethod().generateMethodString();
	}

	@Override
	public Method generateServiceImplMethod() {
		Method method = new Method();
		method.setStandardName("update"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".database.DBConf");
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".daoimpl."+this.domain.getStandardName()+"DaoImpl");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		
		//Service method
		Method daomethod = this.generateDaoMethodDefinition();
				
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementListGenerator.generateServiceImplReturnBoolean(1000L, 2, InterVarUtil.DB.connection, InterVarUtil.DB.dbconf, InterVarUtil.DB.dao, daomethod));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() {
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public Update(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("Update"+StringUtil.capFirst(this.domain.getStandardName()));
	}
	
	public Update(){
		super();
	}

	@Override
	public String generateControllerMethodStringWithSerial() {
		Method m = this.generateControllerMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateFacadeMethod() {
		Method method = new Method();
		method.setStandardName("processRequest");
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		List<String> list = new ArrayList<String>();
		list.add("ServletException");
		list.add("IOException");
		method.setIsprotected(true);
		method.setOtherExceptions(list);
		method.addSignature(new Signature(1,"request",new Type("HttpServletRequest","javax.servlet.http")));
		method.addSignature(new Signature(2,"response",new Type("HttpServletResponse","javax.servlet.http")));
		method.addAdditionalImport("java.io.IOException");
		method.addAdditionalImport("java.io.PrintWriter");
		method.addAdditionalImport("javax.servlet.ServletException");
		method.addAdditionalImport("javax.servlet.http.HttpServlet");
		method.addAdditionalImport("javax.servlet.http.HttpServletRequest");
		method.addAdditionalImport("javax.servlet.http.HttpServletResponse");
		method.addAdditionalImport("java.util.Map");
		method.addAdditionalImport("java.util.TreeMap");
		method.addAdditionalImport("net.sf.json.JSONObject");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addAdditionalImport(this.domain.getPackageToken()+".serviceimpl."+this.domain.getStandardName()+"ServiceImpl");
		
		List<Writeable> wlist = new ArrayList<Writeable>();
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));		
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		wlist.add(NamedStatementGenerator.getFacadeSetContentType(1000L, 2, InterVarUtil.Servlet.response, InterVarUtil.SimpleJEE.UTF8.getVarName()));
		wlist.add(NamedStatementGenerator.getControllerPrintWriterOut(2000L, 2, InterVarUtil.Servlet.response, InterVarUtil.Servlet.out));
		wlist.add(NamedStatementGenerator.getJsonResultMap(3000L, 2, resultMap));
		wlist.add(NamedStatementGenerator.getTryHead(4000L, 2));
		wlist.add(NamedStatementGenerator.getPrepareDomainVarInit(5000L, 3, this.domain));
		wlist.add(NamedStatementListGenerator.generateSetDomainDataFromRequest(6000L, 3, this.domain, InterVarUtil.Servlet.request));
		wlist.add(NamedStatementGenerator.getPrepareService(7000L,3, service));
		wlist.add(NamedStatementGenerator.getCallServiceMethodAssignSuccess(8000L, 3, service, generateServiceMethodDefinition()));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAssignAndNull(9000L, 3, resultMap));
		wlist.add(NamedStatementGenerator.getEncodeMapToJsonResultMap(10000L, 3, resultMap,InterVarUtil.Servlet.out));
		wlist.add(NamedStatementListGenerator.generateCatchExceptionPrintStackPrintJsonMapFinallyCloseOutFooter(110000L, 2, InterVarUtil.Servlet.response,resultMap, InterVarUtil.Servlet.out));
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("Update"+domain.getCapFirstDomainName());
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "{"));
		sl.add(new Statement(2000,1, "text:'编辑',"));
		sl.add(new Statement(3000,1, "iconCls:'icon-edit',"));
		sl.add(new Statement(4000,1, "handler:function(){ "));
		sl.add(new Statement(5000,2, "var rows = $(\"#dg\").datagrid(\"getChecked\");"));
		sl.add(new Statement(6000,2, "if (rows == undefined || rows == null || rows.length == 0 ){"));
		sl.add(new Statement(7000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(8000,3, "return;"));
		sl.add(new Statement(9000,2, "}"));	
		sl.add(new Statement(10000,2, "if (rows.length > 1) {"));
		sl.add(new Statement(11000,3, "$.messager.alert(\"警告\",\"请选定一条记录！\",\"warning\");"));
		sl.add(new Statement(12000,3, "return;"));
		sl.add(new Statement(13000,2, "}"));
		sl.add(new Statement(13500,2, "$(\"#ffedit\").find(\"#"+this.domain.getDomainId().getLowerFirstFieldName()+"\").val(rows[0][\""+this.domain.getDomainId().getLowerFirstFieldName()+"\"]);"));
		long serial = 14000;
		for (Field f: domain.getFieldsWithoutIdAndActive()){
			if (f instanceof Dropdown){
				sl.add(new Statement(serial,2, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").combobox(\"setValue\",rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
			}else{
				sl.add(new Statement(serial,2, "$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").textbox(\"setValue\",rows[0][\""+f.getLowerFirstFieldName()+"\"]);"));
			}
			serial+=1000;
		}			
		sl.add(new Statement(serial,2, "var checkboxs = $(\"#ffedit\").find(\"input[name='active']\");"));
		sl.add(new Statement(serial+100,2, "for (var i=0;i<checkboxs.size();i++){"));
		sl.add(new Statement(serial+200,3, "if (checkboxs.get(i).value == \"\"+rows[0][\"active\"]) checkboxs.get(i).checked=true;"));
		sl.add(new Statement(serial+500,2, "}"));
		sl.add(new Statement(serial+1000,2, "$('#wupdate"+domain.getCapFirstDomainName()+"').window('open');"));
		sl.add(new Statement(serial+2000,1, "}"));
		sl.add(new Statement(serial+3000,0, "}"));
		block.setMethodStatementList(sl);
		return block;		
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		Domain domain = this.domain;
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("update"+domain.getCapFirstDomainName());
				
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "$.ajax({"));
		sl.add(new Statement(2000,2, "type: \"post\","));
		sl.add(new Statement(3000,2, "url: \"../facade/update"+domain.getCapFirstDomainName()+"Facade\","));
		sl.add(new Statement(4000,2, "data:{"));
		long serial = 5000;
		for (Field f: domain.getFields()){
			if (f instanceof Dropdown){
				sl.add(new Statement(serial,3, f.getLowerFirstFieldName()+":$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").combobox(\"getValue\"),"));
			} else if (f.getFieldType().equalsIgnoreCase("boolean")){
				sl.add(new Statement(serial,3, f.getLowerFirstFieldName()+":parseBoolean($(\"#ffedit\").find(\"input[name='"+f.getLowerFirstFieldName()+"']:checked\").val()),"));	
			} else {
				sl.add(new Statement(serial,3, f.getLowerFirstFieldName()+":$(\"#ffedit\").find(\"#"+f.getLowerFirstFieldName()+"\").val(),"));
			}
			serial+=1000;
		}
		sl.add(new Statement(serial,2, "},"));
		sl.add(new Statement(serial+3000,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(serial+4000,3, "if (data.success){"));
		sl.add(new Statement(serial+5000,4, "$('#ffedit').form('clear');"));
		sl.add(new Statement(serial+6000,4, "$(\"#ffedit\").find(\"input[name='"+domain.getActive().getLowerFirstFieldName()+"']\").get(0).checked = true;"));
		sl.add(new Statement(serial+7000,4, "$(\"#wupdate"+domain.getCapFirstDomainName()+"\").window('close');"));
		sl.add(new Statement(serial+8000,4, "$(\"#dg\").datagrid(\"load\");"));
		sl.add(new Statement(serial+9000,3, "}"));
		sl.add(new Statement(serial+10000,2, "},"));
		sl.add(new Statement(serial+11000,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(serial+12000,2, "},"));
		sl.add(new Statement(serial+13000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(serial+14000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(serial+15000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(serial+16000,2, "}"));
		sl.add(new Statement(serial+17000,1, "}); "));
		
		method.setMethodStatementList(sl);
		return method;
		
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}
}
