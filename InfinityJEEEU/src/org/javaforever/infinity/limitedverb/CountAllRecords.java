package org.javaforever.infinity.limitedverb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.domain.Signature;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.generator.NamedStatementGenerator;
import org.javaforever.infinity.generator.NamedStatementListGenerator;
import org.javaforever.infinity.utils.InterVarUtil;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.TableStringUtil;
import org.javaforever.infinity.utils.WriteableUtil;

public class CountAllRecords extends NoControllerVerb {

	@Override
	public Method generateDaoImplMethod(){	
		Method method = new Method();
		method.setStandardName("countAll"+StringUtil.capFirst(this.domain.getStandardName())+"Records");
		method.setReturnType(new Type("Long"));
		method.addAdditionalImport("java.sql.Connection");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,"connection",new Type("Connection","java.sql"),"java.sql"));
		method.setThrowException(true);
		
		List<Writeable> list = new ArrayList<Writeable>();
		Var recordCount = new Var("recordcount",new Type("Long"));
		Var countNum = new Var("countNum",new Type("Long"));
		list.add(NamedStatementGenerator.getCountRecordSqlStatement(100L, 2, domain, InterVarUtil.DB.query, recordCount, countNum));
		list.add(NamedStatementGenerator.getPrepareStatement(200L,2,InterVarUtil.DB.ps, InterVarUtil.DB.query, InterVarUtil.DB.connection));
		list.add(NamedStatementGenerator.getPrepareStatementExcuteQuery(300L,2, InterVarUtil.DB.result, InterVarUtil.DB.ps));
		list.add(new Statement(400,2,recordCount.generateTypeVarString() +" = 0L;"));
		list.add(NamedStatementGenerator.getResultSetWhileNextLoopHead(500L,2, InterVarUtil.DB.result));  
		list.add(NamedStatementGenerator.getRecordCountVarTypeFromResultSet(800L,2+1,domain, InterVarUtil.DB.result,recordCount,countNum));
		list.add(new Statement(900,2,"}"));
		list.add(NamedStatementGenerator.getReturnInt(1000,2, recordCount));
		
		
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() {
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() {
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() {
		Method method = new Method();
		method.setStandardName("countAll"+this.domain.getCapFirstDomainName()+"Records");
		method.setReturnType(new Type("Long"));
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceMethodDefinition() {
		Method method = new Method();
		method.setStandardName("countAll"+this.domain.getCapFirstDomainName()+"Records");
		method.setReturnType(new Type("Long"));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.setThrowException(true);
		
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() {
		Method method = new Method();
		method.setStandardName("countAll"+this.domain.getCapFirstDomainName()+"Records");
		method.setReturnType(new Type("Long"));
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.setThrowException(true);
		method.addMetaData("Override");		
		
		Method daomethod = this.generateDaoMethodDefinition();		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementListGenerator.generateServiceImplReturnInt(1000L, 2, InterVarUtil.DB.connection, InterVarUtil.DB.dbconf, this.domain, InterVarUtil.DB.dao, daomethod));
		method.setMethodStatementList(WriteableUtil.merge(list));

		return method;
	}

	@Override
	public String generateServiceImplMethodString(){
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public CountAllRecords(Domain domain){
		super();
		this.domain = domain;
		this.verbName = "countAll"+this.domain.getCapFirstDomainName()+"Records";
	}
	
	public CountAllRecords(){
		super();
	}

}
