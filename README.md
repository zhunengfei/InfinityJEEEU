# 无垠式Java代码生成器JEEEU版

#### 项目介绍
这是最轻量级的代码生成器，生成物大概占用7.5M数据大小。 采用Servlet,JSP, JSON, Easy UI等简单技术实现，是标准的Model2的MVC设计模式的架构。包含JQuery Easy UI的资源文件和全部示例。是喜欢使用低配服务器的程序员和运维人员的最佳选择。 这是无垠式Java通用代码生成器的最新成员，纤量极速，令人爱不释手。 1.5版研发代号Ada，纪念世界上第一位程序员 Ada Lovelace。采用了和平之翼代码生成器SMEU版的JQuery Easy UI 界面，比无垠式代码生成器SimpleJEE 1.0版美观。
本代码生成器支持SGS(标准生成器脚本）和Excel模板代码生成。同时支持两者的初始数据导入。并会在代码生成物中自动保存原始的SGS脚本文件或Excel源文件。
现在正在Beta 3测试阶段，功能已完整，特性冻结。欢迎大家在附件处下载二进制Beta 3版，并反馈测试结果。

![输入图片说明](https://gitee.com/uploads/images/2018/0506/143426_aab5f000_1203742.jpeg "ada_jeeeu.jpg")


生成器界面：
![输入图片说明](https://gitee.com/uploads/images/2018/0506/171701_018313d6_1203742.jpeg "jeeeu.jpg")

Excel模版生成界面：
![输入图片说明](https://gitee.com/uploads/images/2018/0506/171739_3f2a4e7c_1203742.png "excel.png")

Excel模板截图：
![输入图片说明](https://gitee.com/uploads/images/2018/0526/192237_8d44e0bf_1203742.png "excel.png")

Excel模板截图2：
![输入图片说明](https://gitee.com/uploads/images/2018/0526/192250_c7eec6d9_1203742.png "excel2.png")

代码生成物：
![输入图片说明](https://gitee.com/uploads/images/2018/0506/214741_dd167b49_1203742.png "result0.png")

生成物，多对多
![输入图片说明](https://gitee.com/uploads/images/2018/0506/171800_b9d9bcd0_1203742.png "result.png")

生成物，一对多
![输入图片说明](https://gitee.com/uploads/images/2018/0527/214425_dac9d7e4_1203742.png "dropdown.png")


#### 软件架构
软件架构说明
JQuery Easy UI,
JSON,
JavaScript,
HTML5,
Servlet,
JDBC,
Mysql/MariaDB(2.0将支持Oracle)


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)